# Welcome

## 0. Welcome
欢迎加入天主教小助手产品组^_^
开发同学请按照下面步骤操作，以下步骤要进行邮箱申请，gitlab注册，git配置，代码下载

## 1.邮箱申请
微信@李亚科 加入 zhuri.org 邮箱。邮箱需要提供QQ号，zhuri.org 邮箱使用的是QQ邮箱，以后通过QQ邮箱登录。
## 2.Gitlab注册
用zhuri.org邮箱 注册gitlab，注册链接 https://gitlab.com/users/sign_in#register-pane      验证码可能需要翻墙。注册成功后，@李亚科 申请开发者权限。
## 3.Gitlab配置
###### 1.下载git
* Windows： https://git-scm.com/downloads
* mac和Linux系统自带，不用下载。

###### 2.git账户和邮箱配置
已经配置过的跳过。
1.

* Windows : 右键鼠标，选择 “Git Bash here”
* MacOS或Linux直接打开Terminal

###### 3.依次输入以下命令
git config --global user.name "your name"
git config --global user.email "username@zhuri.org" 
ssh-keygen -t rsa -C "username@zhuri.org" 
回车
回车
回车

###### 4.ssh key配置。输入以下命令
cat ~/.ssh/id_rsa.pub
复制以下内容：
ssh-ras  @￥%&%*~!叽里咕噜

打开gitlab，右上角头像-->Settings-->SSH Keys 
在Key处粘贴刚才复制的内容，Title处随意填写，或者默认也可以。
点击Add key
## 4.下载代码
各个项目地址： https://gitlab.com/dashboard/projects

| 项目       | 描述      | 开发工具  | 负责人  | 
| -----------|:--------:| -----:|-----:|
| Easter       | 网站以及后台      | Intellij Idea  | 李亚科  | 
| CathAssist-Android       | 小助手Android客户端      | Android Studio  | 靳鹏飞  |
| CathAssist-iOS       | 小助手iOS客户端      | XCode  | 姚近海，贾旭辉  | 
| 补充       |       |   |   |

使用Git 下载代码。
开始愉快的写代码吧～

## 5.Git相关：
git管理可以使用命令或者工具（SmartGit , SourceTree）,无论使用命令还是工具，都要了解

* git基础： https://www.cnblogs.com/chenwolong/p/GIT.html
* git分支： http://blog.jobbole.com/109466/
另外Idea自带git管理，也可以使用。

所有项目的开发分支为develop分支，master分支暂不使用。

## 6.Android项目看，其他项目忽略
Android项目，实行 gitlab 的 code review。请详看上面的git分支的文章。为保证代码质量，develop分支的push权限已经关闭，只能通过code review后merge进develop。
所有feature和bugfix基于develop，例如一个new feature，请在develop拉取一个新分支，如下：

![](http://legendtkl.com/img/uploads/2016/feature-branch.png) 

可以在此分支上做代码的提交，例如上图有3个提交，待feature完成，将该分支push到origin。
进入gitlab项目目录，进入Merge Requests。点击new merge request ,在source branch 中选择需要合入develop分支的分支名称。点击Compare branches and continue.
填写title，description(如果title已经描述清楚，忽略)。Assignee选择jinpengfei
Labels有四项：
* bug
* documentation 
* feature
* refactor

请按照提交的代码做的修改类型选择。
点击Submit merge request。
可以在项目下Merge Requests下看到提交的merge申请，其他开发者可以查看代码，点赞或者点踩，在代码中可以标注需要做修改的地方。commiter再做提交完成发现的这些问题后，再做提交，重复上述，直到没有问题后合入develop。


